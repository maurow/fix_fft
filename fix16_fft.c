/* fix16_fft.c - 16-bit fixed-point in-place Fast Fourier Transform.
   See fix_fft.c for the (type agnostic) code.*/

#include "fix16_fft.h"  // This gets the header for 16-bit fixed-point math.
#include "fix_fft.ccc"    // This gets the code.
