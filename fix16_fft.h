/* Header file for 16-bit fixed point  FFT */
#ifndef FIX16_FFT_H
#define FIX16_FFT_H
#warning "C Preprocessor got to fix16_fft.h"

// sets 16-bit fixed point math
#define FFT_BITS 16

#include "fix_fft.hhh" // include type-agnositc header file

#endif
