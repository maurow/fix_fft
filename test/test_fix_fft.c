
/*
  Short test program to accompany fixer_fft.c.

  This test program is not meant to be run on an arduino but on a PC.
*/

#define DEBUG 3


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#include "fix16_fft.h"
//#include "fix8_fft.h"

#define NN        32 //128  // problem size, power of 2
#define PI        3.1415926535

// returns a wave, additive
void coswave(int nn, fft_type xx[], int amp, int freq, int phase){
  int i;
  for (i=0; i<nn; i++)
    xx[i] = round(amp*cos(i*freq*(2*PI)/nn + phase)) + xx[i];
}

void fillrand(int nn, fft_type xx[], int amp){
  int i;
  for (i=0; i<nn; i++)
    xx[i] = rand() % amp;
}

void printvec(int nn, fft_type vec[]){
  int i;
#if DEBUG > 2
  for (i=0; i <nn; i++){
  if (FFT_BITS==8) {
  printf("%4d", vec[i]);}
  else {
  printf("%7d", vec[i]);}
  }
  puts("");
#endif
}

void printerr(int nn, fft_type one[], fft_type two[]){
  fft_type max=0, maxerr=0;
    float relerr=0.0, relmeanerr=0.0,  meanerr=0.0;
  float maxrelerr=0.0;
  int i;
  // find error
  for (i=0; i<nn; i++){
    max = (float) abs(one[i]) > max ? (float) abs(one[i]) : max; 
    maxerr =  abs(one[i]-two[i]) > maxerr ? abs(one[i]-two[i]) : maxerr;
    meanerr += (float) abs(one[i]-two[i]);
  }
  meanerr /= nn;
  maxrelerr = (float) maxerr/max;


  puts("");
  puts("iFFT(FFT)) errors:");
  printf("Max error: %d\n", maxerr);
  printf("Max rel error: %f\n", maxrelerr);
  printf("Mean error: %f\n", meanerr);
  printf("Mean rel error: %f\n", meanerr/max);
}

int main()
{
  int i=1, scale, log2N=0;  // index, scale factor, log of problem size
  unsigned int diff, maxerr=0, meanerr=0;
  fft_type xx[NN] ={0}, xxorig[NN]={0}, im[NN]={0}, ampvec[NN]={0};

  int amp= 1<<(FFT_BITS-3);
  amp = 60;


  // initialize random number generator
#ifdef ARDUINO  // Arudino doesn't have the time function
  srand(1);
#else
  srand(time(NULL));
#endif

  // setup
  while (1) { // figure out log2N
      i *= 2;  // the current power of two
      log2N++; // what the log2 of it is
      if (i==NN) break;
      if (i>NN) {
        printf("NN not a power of two\n");
        return -1;
      }
  }
  
  ////////////////
  // simple example
  coswave(NN, xx, amp, 2, 0);
  coswave(NN, xx, amp, 5, 0);

  printvec(NN, xx);

  puts("");
  scale = FIX_FFT(xx, im, log2N, 0);
  if (scale==-1){
    puts("error");
    return(-1);
  }
  GETPOWER(ampvec, xx, im, log2N);
  printvec(NN/2, ampvec);
  printvec(NN/2, xx);
  printvec(NN/2, im);
  //  return(0);

  ///////////////////
  // test inverse
  puts("Testing with random data");
  /* coswave(NN, xx, amp, 2, 0); */
  /* coswave(NN, xx, amp, 5, 0); */
  fillrand(NN, xx, amp);  // white noise
  for (i=0; i<NN; i++) {
    xxorig[i] = xx[i];
    im[i] = 0;
  }
  scale = FIX_FFT(xx, im, log2N, 0);
  if (scale==-1){
    puts("error");
    return(-1);
  }
  scale = FIX_FFT(xx, im, log2N, 1);
  if (scale==-1){
    puts("error");
    return(-1);
  }
  printf("scale = %d\n", scale);
  printerr(NN, xx, xxorig);
  printvec(NN, xx);
  printvec(NN, im);
  return(0);
  
  
}

