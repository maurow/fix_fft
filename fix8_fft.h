/* Header file for 8-bit fixed point  FFT */
#ifndef FIX8_FFT_H
#define FIX8_FFT_H
#warning "C Preprocessor got to fix8_fft.h"

// sets 8-bit fixed point math
#define FFT_BITS 8


#include "fix_fft.hhh" // include type-agnositc header file

#endif
